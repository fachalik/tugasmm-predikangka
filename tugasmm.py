# -*- coding: utf-8 -*-
"""
Created on Thu Mar 19 21:59:01 2020

@author: chali
"""
# variabel global
ll = []
guess = 500
tries = 1
modif = [500, 250, 125, 63, 32, 16, 8, 4, 2, 1]
berhenti = False

# fungsi memulai game hanya tampilan aja
def newgame():
    print("")
    print(guess)
    print("Pilihan :")
    print("1. Angka Anda Lebih besar dari", guess)
    print("2. Angka Anda Lebih Kecil dari", guess)
    print("3. Angka Anda Sudah Betul")

# fungsi pertambahan. ditambah dengan list modif sesuai dengan tries
def higher(guess, tries):
    asd = guess + modif[tries]
    return asd

# fungsi pengurangan. dikurang dengan list modif sesuai dengan tries
def lower(guess, tries):
    asd =  guess - modif[tries]
    return asd

#percabangan didalam iterasi
def iterasi(guess, tries):
    pil = input("Pilihan Anda :")
    if pil == "1":
        #memanggil fungsi higher
        dsa = higher(guess, tries)
        if dsa == 1000:
            print("")
            print(dsa)
            print("Pilihan :")
            print("2. Angka Anda Lebih Kecil dari", dsa)
            print("3. Angka Anda Sudah Betul")
        else:
            print("")
            print(dsa)
            print("Pilihan :")
            print("1. Angka Anda Lebih besar dari", dsa)
            print("2. Angka Anda Lebih Kecil dari", dsa)
            print("3. Angka Anda Sudah Betul")
        guess = dsa

    elif pil == "2":
        #memanggil fungsi lower
        dsa = lower(guess, tries)
        if dsa == 0:
            print("")
            print(dsa)
            print("Pilihan :")
            print("1. Angka Anda Lebih besar dari", dsa)
            print("3. Angka Anda Sudah Betul")
        else: 
            print("")
            print(dsa)
            print("Pilihan :")
            print("1. Angka Anda Lebih besar dari", dsa)
            print("2. Angka Anda Lebih Kecil dari", dsa)
            print("3. Angka Anda Sudah Betul")
        guess = dsa
        
        
    
    ll.append(guess)
    ll.append(pil)
    return ll

# iterasi dilakukan sebanyak 10x jika betul maka akan berhenti
newgame()
#berhenti jika True
while berhenti != True:
    #berhenti jika sama dengan 11
    while tries < 11:
        #memanggil fungsi iterasi
        ss = iterasi(guess,tries)
        #assign list ss[0] atau sama dengan guess yang sudah diproses pada higher/lower yang
        #terdapat pada fungsi iterasi
        guess = ss[0]
        #jika ll[1] atau variabel "pil" pada fungsi iterasi sama dengan 3 maka akan berhenti 
        if  ll[1] == "3":
            print("")
            print("Angka Anda Adalah : ", guess)
            break
        tries += 1
        ll = []
        #berhenti perulangan ke dua berhenti jika sama dengan 10
        if tries == 10:
            break
    print("")
    if tries == 10:
        print("Angka anda Adalah :", guess,)
        print("atau")
        print("Anda pasti memikirkan angka baru pada saat memainkan game ini")
    print("Berhenti!")
    print("1. Ya")
    print("2. Tidak")
    pil2 = input("berhenti ?")
    #perulangan pertama berhenti jika pil2 sama dengan 1
    if pil2 == "1":
        berhenti == True
        break
    #perulangan berlanjut jika pil2 sama dengan 2
    #kemabali ke semula
    if pil2 == "2":
        berhenti == False
        guess = 500
        tries = 1
        ll = []
        newgame()